#ifndef CONNECTOR_H_
#define CONNECTOR_H_

#include <stdint.h>

class Connector
{
public:
  Connector ();
  virtual ~Connector ();
  virtual int read(uint8_t** raw_data) = 0;
  virtual int write(uint8_t* raw_data, uint8_t data_length) = 0;
};

#endif
