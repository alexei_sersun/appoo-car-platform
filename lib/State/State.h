#ifndef STATE_H_
#define STATE_H_

#include <stdint.h>

class State {
private:
  uint8_t state_;

public:
  State ();
  virtual ~State ();
  void set(uint8_t state);
  uint8_t get();

  static const uint8_t CONNECTED = 0x10;
  static const uint8_t DISCONNECTED = 0x00;
};
#endif
