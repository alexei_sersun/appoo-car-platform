#include "State.h"

State::State ()
{
  state_ = DISCONNECTED;
}

State::~State ()
{

}

void State::set(uint8_t state)
{
  if (!(state == CONNECTED || state == DISCONNECTED)) return;
  state_ = state;
}

uint8_t State::get()
{
  return state_;
}
