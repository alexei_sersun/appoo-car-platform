#include "MadDrivingModeFactory.h"

MadDrivingModeFactory::MadDrivingModeFactory (Sensor* avoidance_sensor)
{
  manual_mode_ = new ManualMode();
  avoidance_mode_ = new AvoidanceMode(avoidance_sensor);
}

MadDrivingModeFactory::~MadDrivingModeFactory ()
{
  delete manual_mode_;
  delete avoidance_mode_;
}

DrivingMode* MadDrivingModeFactory::getInstance(uint8_t mode)
{
  switch (mode) {
    case MANUAL: return manual_mode_;
    case AVOIDANCE: return avoidance_mode_;
    default: return manual_mode_;
  }
}

MadDrivingModeFactory* MadDrivingModeFactory::getInstance(Sensor* avoidance_sensor)
{
  if (!factory_)
  {
    factory_ = new MadDrivingModeFactory(avoidance_sensor);
  }
  return factory_;
}

MadDrivingModeFactory* MadDrivingModeFactory::factory_;