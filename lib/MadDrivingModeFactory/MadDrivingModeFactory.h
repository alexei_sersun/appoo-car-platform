#ifndef MADDRIVINGMODEFACTORY_H_
#define MADDRIVINGMODEFACTORY_H_

#include <stdint.h>
#include "Sensor.h"
#include "DrivingMode.h"
#include "AvoidanceMode.h"
#include "ManualMode.h"
#include "AbstractDrivingModeFactory.h"

class MadDrivingModeFactory : public AbstractDrivingModeFactory
{
private:
  AvoidanceMode* avoidance_mode_;
  ManualMode* manual_mode_;
  MadDrivingModeFactory (Sensor* avoidance_sensor = new Sensor(12));
  static MadDrivingModeFactory* factory_;
  
public:
  MadDrivingModeFactory(MadDrivingModeFactory const&);
  void operator=(MadDrivingModeFactory const&);

  static MadDrivingModeFactory* getInstance(Sensor* avoidance_sensor = new Sensor(12));
  virtual ~MadDrivingModeFactory ();
  DrivingMode* getInstance (uint8_t mode);

  const static uint8_t MANUAL = 0x0;
  const static uint8_t AVOIDANCE = 0x1;
};

#endif
