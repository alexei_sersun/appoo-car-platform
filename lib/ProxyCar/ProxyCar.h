#ifndef PROXYCAR_H_
#define PROXYCAR_H_

#include <Car.h>
#include <Chassis.h>
#include <Mediator.h>
#include <Connector.h>
#include <AbstractCmdInterpreter.h>
#include <AbstractDrivingModeFactory.h>
#include <Sensor.h>
#include <State.h>
#include <AbstractLogger.h>

class ProxyCar : public Car
{
private:
    Car* car_;

    Chassis* chassis_;
    Mediator* controller_;
    Connector* connector_;
    Sensor* proximity_sensor_;
    
public:
    ProxyCar(Car* car);
    ~ProxyCar();
    void run();
    void setChassis(Chassis*);
    void setController(Mediator*);
    void setConnector(Connector*);
    void setCmdInterpreter(AbstractCmdInterpreter*);
    void setDrivingModeFactory(AbstractDrivingModeFactory*);
    void setSensor(Sensor*);
    void setState(State*);
    void setLogger(AbstractLogger*);
};

#endif
