#include "ProxyCar.h"

ProxyCar::ProxyCar(Car* car)
{
    car_ = car;
}

ProxyCar::~ProxyCar()
{

}

void ProxyCar::run()
{
    car_->run();
}

void ProxyCar::setChassis(Chassis* chassis)
{
    if (chassis_ != nullptr) return;
    chassis_ = chassis;
    car_->setChassis(chassis);
}

void ProxyCar::setController(Mediator* controller)
{
    if (controller_ != nullptr) return;
    controller_ = controller;
    car_->setController(controller);
}

void ProxyCar::setConnector(Connector* connector)
{
    if (connector_ != nullptr) return;
    connector_ = connector;
    car_->setConnector(connector);
}

void ProxyCar::setCmdInterpreter(AbstractCmdInterpreter* interpreter)
{
    car_->setCmdInterpreter(interpreter);
}

void ProxyCar::setDrivingModeFactory(AbstractDrivingModeFactory* driving_mode_factory)
{
    car_->setDrivingModeFactory(driving_mode_factory);
}

void ProxyCar::setSensor(Sensor* sensor)
{
    if (proximity_sensor_ != nullptr) return;
    proximity_sensor_ = sensor;
    car_->setSensor(sensor);
}

void ProxyCar::setState(State* state)
{
    car_->setState(state);
}

void ProxyCar::setLogger(AbstractLogger* logger)
{
    car_->setLogger(logger);
}
