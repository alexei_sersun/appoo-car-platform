#ifndef ABSTRACT_LOGGER_H_
#define ABSTRACT_LOGGER_H_

class AbstractLogger
{
private:
    /* data */
public:
    AbstractLogger(/* args */);
    virtual ~AbstractLogger();
    virtual void log() = 0;
    virtual void timedLog() = 0;
};

#endif
