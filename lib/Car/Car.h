#ifndef CAR_H_
#define CAR_H_

#include <Chassis.h>
#include <Mediator.h>
#include <Connector.h>
#include <AbstractCmdInterpreter.h>
#include <AbstractDrivingModeFactory.h>
#include <Sensor.h>
#include <State.h>
#include <AbstractLogger.h>

class Car
{
public:
    Car();
    virtual ~Car();
    virtual void run() = 0;
    virtual void setChassis(Chassis*) = 0;
    virtual void setController(Mediator*) = 0;
    virtual void setConnector(Connector*) = 0;
    virtual void setCmdInterpreter(AbstractCmdInterpreter*) = 0;
    virtual void setDrivingModeFactory(AbstractDrivingModeFactory*) = 0;
    virtual void setSensor(Sensor*) = 0;
    virtual void setState(State*) = 0;
    virtual void setLogger(AbstractLogger*) = 0;
};

#endif
