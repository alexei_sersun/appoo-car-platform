#ifndef DRIVINGMODE_H_
#define DRIVINGMODE_H_

class DrivingMode
{
private:
  /* data */

public:
  DrivingMode ();
  virtual ~DrivingMode ();
  virtual bool canMoveForward() = 0;
};

#endif
