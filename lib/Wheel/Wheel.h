#ifndef WHEEL_H_
#define WHEEL_H_

#include <stdint.h>

class Wheel
{
private:
  uint8_t power_pin1_;
  uint8_t power_pin2_;
  uint8_t speed_pin_;
  uint8_t speed_;
  uint8_t threshold_;

public:
  Wheel (uint8_t power_pin1, uint8_t power_pin2, uint8_t speed_pin, uint8_t threshold = 30);
  virtual ~Wheel ();
  void stop();
  void setSpeed(uint8_t);
};

#endif
