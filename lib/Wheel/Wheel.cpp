#include "Wheel.h"
#include <Arduino.h>

Wheel::Wheel (uint8_t power_pin1, uint8_t power_pin2, uint8_t speed_pin, uint8_t threshold)
{
  power_pin1_ = power_pin1;
  power_pin2_ = power_pin2;
  speed_pin_ = speed_pin;
  threshold_ = threshold;
  pinMode(power_pin1_, OUTPUT);
  pinMode(power_pin2_, OUTPUT);
  pinMode(speed_pin_, OUTPUT);
}

Wheel::~Wheel ()
{

}

void Wheel::stop()
{
  setSpeed(0);
  digitalWrite(power_pin1_, LOW);
  digitalWrite(power_pin2_, LOW);
}

void Wheel::setSpeed(uint8_t speed)
{
  bool move_forward = speed >= 128;
  uint8_t centered_speed = abs(speed - 128);
  uint8_t applied_speed = centered_speed > threshold_ ? centered_speed * 2 : 0;
  digitalWrite(power_pin1_, move_forward? LOW : HIGH);
  digitalWrite(power_pin2_, move_forward? HIGH : LOW);
  analogWrite(speed_pin_, applied_speed);
}
