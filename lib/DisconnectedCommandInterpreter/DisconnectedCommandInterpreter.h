#ifndef DISCONNECTEDCOMMANDINTERPRETER_H_
#define DISCONNECTEDCOMMANDINTERPRETER_H_

#include <stdint.h>
#include "Command.h"
#include "AbstractDrivingModeFactory.h"
#include <AbstractCmdInterpreter.h>
#include <Mediator.h>
#include <LogFactory.h>

class DisconnectedCommandInterpreter : public AbstractCmdInterpreter
{
private:
  LogFactory* log_factory_;
  Mediator* controller_;
  Command* parse(uint8_t* raw_data, uint8_t data_length);
  void execute(Command*);

public:
  DisconnectedCommandInterpreter(Mediator* mediator);
  virtual ~DisconnectedCommandInterpreter ();
  void interpret(uint8_t* raw_data, uint8_t data_length);
};

#endif
