#include <Arduino.h>
#include "DisconnectedCommandInterpreter.h"
#include "DrivingModeCommand.h"
#include "DummyCommand.h"
#include "SetSpeedCommand.h"
#include "SteerCommand.h"
#include "StopCommand.h"
#include "WrongCommand.h"
#include "ConnectCommand.h"
#include "DisconnectCommand.h"
#include "SimpleLogDecorator.h"

Command* DisconnectedCommandInterpreter::parse(uint8_t* raw_data, uint8_t data_length)
{
  Serial.print("Received ");
  Serial.print(data_length);
  Serial.print(' ');
  for (uint8_t i = 0; i < data_length; i++) {
    Serial.print(raw_data[i], HEX);
  }
  Serial.println();

  if (!(4 < data_length && data_length < 7 && raw_data[0] == 0xFA &&
      raw_data[1] == 0xF1 && raw_data[2] == 0x51))
  {
    Serial.println("Wrong command");
    return log_factory_->decorate(new WrongCommand(controller_));
  }

  if (raw_data[3] == 0x01)
  {
    Serial.println("Connect");
    return log_factory_->decorate(new ConnectCommand(controller_));
  }

  Serial.println("Wrong command");
  return log_factory_->decorate(new WrongCommand(controller_));
}

void DisconnectedCommandInterpreter::execute(Command* command)
{
  command->execute();
  delete command;
}

DisconnectedCommandInterpreter::DisconnectedCommandInterpreter (Mediator* controller)
{
  controller_ = controller;
  log_factory_ = new LogFactory();
}

DisconnectedCommandInterpreter::~DisconnectedCommandInterpreter ()
{
  delete log_factory_;
}

void DisconnectedCommandInterpreter::interpret(uint8_t* raw_data, uint8_t data_length)
{
  if (data_length == 0) return;
  Command* command = parse(raw_data, data_length);
  execute(command);
}
