#include "CarDirector.h"
#include "CarBuilder.h"

CarDirector::CarDirector(CarBuilder* builder)
{
    builder_ = builder;
}

CarDirector::~CarDirector()
{
}

Car* CarDirector::createCar()
{
    builder_->setConnector();
    builder_->setState();
    builder_->setChassis();
    builder_->setSensor();
    builder_->setDrivingModeFactory();
    builder_->setController();
    builder_->setCmdInterpreter();
    builder_->setLogger();
    return builder_->getCar();
}
