#ifndef CARDIRECTOR_H_
#define CARDIRECTOR_H_

#include "Car.h"
#include "CarBuilder.h"

class CarDirector
{
private:
    CarBuilder* builder_;
public:
    CarDirector(CarBuilder*);
    ~CarDirector();
    Car* createCar();
};

#endif
