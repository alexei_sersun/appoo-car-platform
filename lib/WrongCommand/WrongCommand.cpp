#include "WrongCommand.h"
#include <Arduino.h>

WrongCommand::WrongCommand (Mediator* controller)
{
  controller_ = controller;
  message_ = (uint8_t*) malloc(5 * sizeof(uint8_t));
  message_[0] = 0xFA;
  message_[1] = 0xF1;
  message_[2] = 0x51;
  message_[3] = 0x04;
  message_[4] = 0x00;
}

WrongCommand::~WrongCommand ()
{

}

void WrongCommand::execute()
{
  controller_->writeToConnector(message_, 5);
}
