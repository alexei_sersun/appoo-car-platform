#ifndef WRONGCOMMAND
#define WRONGCOMMAND

#include "Command.h"
#include "Mediator.h"

class WrongCommand : public Command
{
private:
  Mediator* controller_;
  uint8_t* message_;

public:
  WrongCommand (Mediator* controller);
  virtual ~WrongCommand ();
  void execute();
};

#endif
