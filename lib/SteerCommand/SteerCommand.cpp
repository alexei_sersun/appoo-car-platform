#include "SteerCommand.h"

SteerCommand::SteerCommand (Mediator* controller, uint8_t steering_degrees)
{
  steering_degrees_ = steering_degrees;
  controller_ = controller;
}

SteerCommand::~SteerCommand ()
{

}

void SteerCommand::execute()
{
  controller_->steerBy(steering_degrees_);
}
