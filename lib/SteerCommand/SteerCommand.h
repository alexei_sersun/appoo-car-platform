#ifndef STEERCOMMAND_H_
#define STEERCOMMAND_H_

#include "Command.h"
#include "Mediator.h"

class SteerCommand : public Command
{
private:
  Mediator* controller_;
  uint8_t steering_degrees_;

public:
  SteerCommand (Mediator* controller, uint8_t steering_degrees);
  virtual ~SteerCommand ();
  void execute();
};

#endif
