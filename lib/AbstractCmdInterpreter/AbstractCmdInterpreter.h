#ifndef ABSTRACT_CMD_INTERPRETER_H_
#define ABSTRACT_CMD_INTERPRETER_H_

#include <stdint.h>

class AbstractCmdInterpreter
{
private:
    /* data */
public:
    AbstractCmdInterpreter(/* args */);
    virtual ~AbstractCmdInterpreter();
    virtual void interpret(uint8_t* raw_data, uint8_t data_length) = 0;
};

#endif
