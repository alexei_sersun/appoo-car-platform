#ifndef SETSPEEDCOMMAND_H_
#define SETSPEEDCOMMAND_H_

#include "Command.h"
#include "Mediator.h"

class SetSpeedCommand : public Command
{
private:
  Mediator* controller_;
  uint8_t speed_;

public:
  SetSpeedCommand (Mediator* controller, uint8_t speed);
  virtual ~SetSpeedCommand ();
  void execute();
};

#endif
