#include "SetSpeedCommand.h"

SetSpeedCommand::SetSpeedCommand(Mediator* controller, uint8_t speed)
{
  speed_ = speed;
  controller_ = controller;
}

SetSpeedCommand::~SetSpeedCommand ()
{

}

void SetSpeedCommand::execute()
{
  controller_->setSpeed(speed_);
}
