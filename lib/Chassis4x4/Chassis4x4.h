#ifndef CHASSIS4X4_H_
#define CHASSIS4X4_H_

#include "Chassis.h"

class Chassis4x4 : public Chassis
{
private:
  /* data */

public:
  Chassis4x4 ();
  virtual ~Chassis4x4 ();
  void stop();
  void setSpeed(uint8_t);
  void steer(uint8_t);
  uint8_t getSpeed();
  uint8_t getSteering();
  void selfCheck();
};

#endif
