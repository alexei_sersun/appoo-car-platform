#ifndef LOG_DECORATOR_H_
#define LOG_DECORATOR_H_

#include <Command.h> 

class LogDecorator : public Command
{
public:
    LogDecorator(Command* command);
    virtual ~LogDecorator();
    virtual void execute() = 0;
};

#endif