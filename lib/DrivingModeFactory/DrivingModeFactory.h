#ifndef DRIVINGMODEFACTORY_H_
#define DRIVINGMODEFACTORY_H_

#include <stdint.h>
#include "Sensor.h"
#include "DrivingMode.h"
#include "AvoidanceMode.h"
#include "ManualMode.h"
#include "AbstractDrivingModeFactory.h"

class DrivingModeFactory : public AbstractDrivingModeFactory
{
private:
  AvoidanceMode* avoidance_mode_;
  ManualMode* manual_mode_;
  DrivingModeFactory (Sensor* avoidance_sensor = new Sensor(12));
  static DrivingModeFactory* factory_;
  
public:
  DrivingModeFactory(DrivingModeFactory const&);
  void operator=(DrivingModeFactory const&);

  static DrivingModeFactory* getInstance(Sensor* avoidance_sensor = new Sensor(12));
  virtual ~DrivingModeFactory ();
  DrivingMode* getInstance (uint8_t mode);

  const static uint8_t MANUAL = 0x0;
  const static uint8_t AVOIDANCE = 0x1;
};

#endif
