#include "DrivingModeFactory.h"

DrivingModeFactory::DrivingModeFactory (Sensor* avoidance_sensor)
{
  manual_mode_ = new ManualMode();
  avoidance_mode_ = new AvoidanceMode(avoidance_sensor);
}

DrivingModeFactory::~DrivingModeFactory ()
{
  delete manual_mode_;
  delete avoidance_mode_;
}

DrivingMode* DrivingModeFactory::getInstance(uint8_t mode)
{
  switch (mode) {
    case MANUAL: return manual_mode_;
    case AVOIDANCE: return avoidance_mode_;
    default: return manual_mode_;
  }
}

DrivingModeFactory* DrivingModeFactory::getInstance(Sensor* avoidance_sensor)
{
  if (!factory_)
  {
    factory_ = new DrivingModeFactory(avoidance_sensor);
  }
  return factory_;
}

DrivingModeFactory* DrivingModeFactory::factory_;