#ifndef SIMPLE_LOG_DECORATOR_H_
#define SIMPLE_LOG_DECORATOR_H_

#include <LogDecorator.h>

class SimpleLogDecorator : public LogDecorator
{
private:
    Command* command_;
public:
    SimpleLogDecorator(Command* command) : LogDecorator(command) { command_ = command; }
    ~SimpleLogDecorator();
    void execute();
};

#endif