#include <SimpleLogDecorator.h>
#include <Arduino.h>

SimpleLogDecorator::~SimpleLogDecorator()
{
    delete command_;
}

void SimpleLogDecorator::execute()
{
    Serial.println("Executing a command.");
    command_->execute();
}