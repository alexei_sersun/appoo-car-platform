#include "RealCar.h"

RealCar::RealCar()
{
}

RealCar::~RealCar()
{
}

void RealCar::run()
{
    controller_->loop();
}

void RealCar::setChassis(Chassis* chassis)
{
    chassis_ = chassis;
}

void RealCar::setController(Mediator* controller)
{
    controller_ = controller;
}

void RealCar::setConnector(Connector* connector)
{
    connector_ = connector;
}

void RealCar::setCmdInterpreter(AbstractCmdInterpreter* interpreter)
{
    interpreter_ = interpreter;
}

void RealCar::setDrivingModeFactory(AbstractDrivingModeFactory* driving_mode_factory)
{
    driving_mode_factory_ = driving_mode_factory;
}

void RealCar::setSensor(Sensor* sensor)
{
    proximity_sensor_ = sensor;
}

void RealCar::setState(State* state)
{
    state_ = state;
}

void RealCar::setLogger(AbstractLogger* logger)
{
    logger_ = logger;
}

