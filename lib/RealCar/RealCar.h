#ifndef REALCAR_H_
#define REALCAR_H_

#include <Car.h>
#include <Chassis.h>
#include <Mediator.h>
#include <Connector.h>
#include <AbstractCmdInterpreter.h>
#include <AbstractDrivingModeFactory.h>
#include <Sensor.h>
#include <State.h>
#include <AbstractLogger.h>

class RealCar : public Car
{
private:
    Chassis* chassis_;
    Mediator* controller_;
    Connector* connector_;
    AbstractCmdInterpreter* interpreter_;
    AbstractDrivingModeFactory* driving_mode_factory_;
    Sensor* proximity_sensor_;
    State* state_;
    AbstractLogger* logger_;

public:
    RealCar();
    ~RealCar();
    void run();
    void setChassis(Chassis*);
    void setController(Mediator*);
    void setConnector(Connector*);
    void setCmdInterpreter(AbstractCmdInterpreter*);
    void setDrivingModeFactory(AbstractDrivingModeFactory*);
    void setSensor(Sensor*);
    void setState(State*);
    void setLogger(AbstractLogger*);
};

#endif
