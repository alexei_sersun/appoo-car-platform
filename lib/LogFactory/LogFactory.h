#ifndef LOGFACTORY_H_
#define LOGFACTORY_H_

#include <stdint.h>
#include <LogDecorator.h>
#include <Command.h>

class LogFactory
{
private:
    uint8_t type_;
public:
    LogFactory(/* args */);
    ~LogFactory();
    LogDecorator* decorate(Command*);
    void setLogLevel(uint8_t);
};

#endif