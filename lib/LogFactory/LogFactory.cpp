#include <LogFactory.h>
#include <SimpleLogDecorator.h>
#include <ExtendedLogDecorator.h>

LogFactory::LogFactory(/* args */)
{
}

LogFactory::~LogFactory()
{
}

LogDecorator* LogFactory::decorate(Command* command)
{
    switch (type_){
        case 0: return new SimpleLogDecorator(command);
        case 1: return new ExtendedLogDecorator(command);
        default: return new SimpleLogDecorator(command);
    }
}

void LogFactory::setLogLevel(uint8_t log_level)
{
    type_ = log_level;
}
