#ifndef DUMMYCOMMAND_H_
#define DUMMYCOMMAND_H_

#include "Command.h"

class DummyCommand : public Command
{
public:
  DummyCommand ();
  virtual ~DummyCommand ();
  void execute();
};

#endif
