#include "DummyCommand.h"
#include <Arduino.h>

DummyCommand::DummyCommand ()
{

}

DummyCommand::~DummyCommand ()
{

}

void DummyCommand::execute()
{
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(13, LOW);
  delay(500);
  digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(13, LOW);
  delay(500);
}
