#ifndef EXTENDED_LOG_DECORATOR_H_
#define EXTENDED_LOG_DECORATOR_H_

#include <LogDecorator.h>

class ExtendedLogDecorator : public LogDecorator
{
private:
    Command* command_;
public:
    ExtendedLogDecorator(Command* command) : LogDecorator(command) { command_ = command; };
    ~ExtendedLogDecorator();
    void execute();
};

#endif