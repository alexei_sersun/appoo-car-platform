#include <ExtendedLogDecorator.h>
#include <Arduino.h>

ExtendedLogDecorator::~ExtendedLogDecorator()
{
    delete command_;
}

void ExtendedLogDecorator::execute()
{
    Serial.print("[");
    Serial.print(millis());
    Serial.println("] Executing a command.");
    command_->execute();
}