#ifndef MEDIATOR_H_
#define MEDIATOR_H_

#include <stdint.h>
#include <AbstractDrivingModeFactory.h>
#include <Connector.h>
#include <Chassis.h>
#include <AbstractCmdInterpreter.h>
#include <AbstractLogger.h>

class Mediator
{
private:
    /* data */
public:
    Mediator();
    ~Mediator();

    virtual void registerCmdInterpreter(AbstractCmdInterpreter*) = 0;
    virtual void registerLogger(AbstractLogger*) = 0;
    virtual void registerChassis(Chassis*) = 0;
    virtual void registerConnector(Connector*) = 0;
    virtual void registerDrivingModeFactory(AbstractDrivingModeFactory*) = 0;
    virtual int writeToConnector(uint8_t* raw_data, uint8_t data_length) = 0;
    virtual void stopChassis() = 0;
    virtual void steerBy(uint8_t steering_degrees) = 0;
    virtual void setSpeed(uint8_t speed) = 0;
    virtual void setDrivingMode(uint8_t mode) = 0;
    virtual uint8_t getSpeed() = 0;
    virtual uint8_t getSteering() = 0;
    virtual void setState(uint8_t state) = 0;
    virtual uint8_t getState() = 0;
    virtual void loop() = 0;
};
#endif