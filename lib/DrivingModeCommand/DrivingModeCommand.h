#ifndef DRIVINGMODECOMMAND_H_
#define DRIVINGMODECOMMAND_H_

#include "Command.h"
#include "AbstractDrivingModeFactory.h"
#include "Mediator.h"

class DrivingModeCommand : public Command
{
private:
  Mediator* controller_;
  uint8_t mode_;
public:
  DrivingModeCommand (Mediator* controller, uint8_t mode);
  virtual ~DrivingModeCommand ();
  void execute();
};

#endif
