#include "DrivingModeCommand.h"
#include "DrivingMode.h"

DrivingModeCommand::DrivingModeCommand (Mediator* controller, uint8_t mode)
{
  controller_ = controller;
  mode_ = mode;
}

DrivingModeCommand::~DrivingModeCommand ()
{

}

void DrivingModeCommand::execute()
{
  controller_->setDrivingMode(mode_);
}
