#ifndef SENSOR_H_
#define SENSOR_H_

#include <stdint.h>

class Sensor
{
private:
  uint8_t sensor_pin_;

public:
  Sensor (uint8_t sensor_pin);
  virtual ~Sensor ();
  uint8_t readData();
};

#endif
