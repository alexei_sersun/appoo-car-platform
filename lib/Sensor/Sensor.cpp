#include "Sensor.h"
#include <Arduino.h>

Sensor::Sensor (uint8_t sensor_pin)
{
  sensor_pin_ = sensor_pin;
  pinMode(sensor_pin_, INPUT);
}

Sensor::~Sensor ()
{

}

uint8_t Sensor::readData()
{
  return (uint8_t) digitalRead(sensor_pin_);
}
