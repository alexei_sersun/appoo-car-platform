#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <stdint.h>
#include "Mediator.h"
#include <AbstractCmdInterpreter.h>
#include "Connector.h"
#include "Chassis.h"
#include <AbstractLogger.h>

class Controller : public Mediator
{
private:
  AbstractCmdInterpreter* interpreter_;
  Connector* connector_;
  AbstractLogger* logger_;
  Chassis* chassis_;
  AbstractDrivingModeFactory* factory_;
  uint8_t* raw_data_;
  uint8_t buffer_size_;
  uint8_t data_length_;

public:
  Controller();
  virtual ~Controller ();
  void registerCmdInterpreter(AbstractCmdInterpreter*);
  void registerLogger(AbstractLogger*);
  void registerChassis(Chassis*);
  void registerConnector(Connector*);
  void registerDrivingModeFactory(AbstractDrivingModeFactory*);
  int writeToConnector(uint8_t* raw_data, uint8_t data_length);
  void stopChassis();
  void steerBy(uint8_t steering_degrees);
  void setSpeed(uint8_t speed);
  void setDrivingMode(uint8_t mode);
  uint8_t getSpeed();
  uint8_t getSteering();
  void setState(uint8_t state);
  uint8_t getState();
  void loop();
};

#endif
