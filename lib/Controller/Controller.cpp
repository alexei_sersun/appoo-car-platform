#include "Controller.h"
#include <Arduino.h>

Controller::Controller ()
{
  buffer_size_ = 10;
  raw_data_ = (uint8_t*) malloc(buffer_size_ * sizeof(uint8_t));
  data_length_ = 0;
}

Controller::~Controller ()
{
  free(raw_data_);
}

void Controller::loop()
{
  data_length_ = connector_->read(&raw_data_);
  interpreter_->interpret(raw_data_, data_length_);
  chassis_->selfCheck();
  if (logger_ != nullptr)
  {
    logger_->timedLog();
  }
}

void Controller::registerCmdInterpreter(AbstractCmdInterpreter* ci)
{
  interpreter_ = ci;
}

void Controller::registerLogger(AbstractLogger* logger)
{
  logger_ = logger;
}

void Controller::registerChassis(Chassis* chassis)
{
  chassis_ = chassis;
}

void Controller::registerConnector(Connector* connector)
{
  connector_ = connector;
}

void Controller::registerDrivingModeFactory(AbstractDrivingModeFactory* factory)
{
  factory_ = factory;
}

int Controller::writeToConnector(uint8_t* raw_data, uint8_t data_length)
{
  connector_->write(raw_data, data_length);
}

void Controller::stopChassis()
{
  chassis_->stop();
}
  
void Controller::steerBy(uint8_t steering_degrees)
{
  chassis_->steer(steering_degrees);
}

void Controller::setSpeed(uint8_t speed)
{
  chassis_->setSpeed(speed);
}

void Controller::setDrivingMode(uint8_t mode)
{
  chassis_->setDrivingMode(factory_->getInstance(mode));
}

uint8_t Controller::getSpeed()
{
  return chassis_->getSpeed();
}

uint8_t Controller::getSteering()
{
  return chassis_->getSteering();
}

void Controller::setState(uint8_t state)
{
  
}

uint8_t Controller::getState()
{
  return 0;
}
