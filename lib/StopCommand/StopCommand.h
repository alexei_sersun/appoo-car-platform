#ifndef STOPCOMMAND_H_
#define STOPCOMMAND_H_

#include "Command.h"
#include "Mediator.h"

class StopCommand : public Command
{
private:
  Mediator* controller_;

public:
  StopCommand (Mediator* controller);
  virtual ~StopCommand ();
  void execute();
};

#endif
