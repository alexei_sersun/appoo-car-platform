#include "StopCommand.h"

StopCommand::StopCommand (Mediator* controller)
{
  controller_ = controller;
}

StopCommand::~StopCommand ()
{

}

void StopCommand::execute()
{
  controller_->stopChassis();
}
