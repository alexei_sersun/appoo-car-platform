#ifndef CARBUILDER_H_
#define MADCARBUILDER_H_

#include "CarBuilder.h"
#include "Car.h"

class MadCarBuilder : public CarBuilder
{
private:
    Car* car_;
    Chassis* chassis_;
    Mediator* controller_;
    Connector* connector_;
    AbstractCmdInterpreter* interpreter_;
    AbstractDrivingModeFactory* driving_mode_factory_;
    Sensor* proximity_sensor_;
    State* state_;
    AbstractLogger* logger_;
    
    bool has_chassis_;
    bool has_controller_;
    bool has_connector_;
    bool has_interpreter_;
    bool has_driving_modes_;
    bool has_proximity_sensor_;
    bool has_state_;
    bool has_logger_;

public:
    MadCarBuilder();
    ~MadCarBuilder();
    Car* getCar();
    void setChassis();
    void setController();
    void setConnector();
    void setCmdInterpreter();
    void setDrivingModeFactory();
    void setSensor();
    void setState();
    void setLogger();
};

#endif
