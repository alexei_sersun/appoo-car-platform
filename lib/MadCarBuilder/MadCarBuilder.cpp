#include "MadCarBuilder.h"
#include "MadDrivingModeFactory.h"
#include <ProxyCar.h>
#include <RealCar.h>
#include <Chassis3x2.h>
#include <BTConnector.h>
#include <Controller.h>
#include <DisconnectedCommandInterpreter.h>
#include <Logger.h>

MadCarBuilder::MadCarBuilder()
{
    car_ = new ProxyCar(new RealCar());
}

MadCarBuilder::~MadCarBuilder()
{

}

Car* MadCarBuilder::getCar()
{
    if (!(has_chassis_ || has_connector_ || has_controller_ || has_driving_modes_ 
            || has_interpreter_ || has_logger_ || has_state_)) return nullptr;
    return car_;
}

void MadCarBuilder::setChassis()
{
    if (has_chassis_) return;
    Chassis* chassis_ = new Chassis3x2(6, 7, 8, 9, 10, 11, nullptr);
    car_->setChassis(chassis_);
    has_chassis_ = true;
}

void MadCarBuilder::setController()
{
    if (has_controller_) return;
    if (!(has_interpreter_ || has_connector_ || has_chassis_ || has_logger_)) return;
    controller_ = new Controller();
    car_->setController(controller_);
    has_controller_ = true;
}
    
void MadCarBuilder::setConnector()
{
    if (has_connector_) return;
    connector_ = new BTConnector(2, 3);
    car_->setConnector(connector_);
    has_connector_ = true;
}
    
void MadCarBuilder::setCmdInterpreter()
{
    if (has_interpreter_) return;
    if (!(has_driving_modes_ || has_connector_ || has_chassis_ || has_state_)) return;
    interpreter_ = new DisconnectedCommandInterpreter(controller_);
    car_->setCmdInterpreter(interpreter_);
    has_interpreter_ = true;
}
    
void MadCarBuilder::setDrivingModeFactory()
{
    if (has_driving_modes_) return;
    if (!has_proximity_sensor_) return;
    driving_mode_factory_ = MadDrivingModeFactory::getInstance(proximity_sensor_);
    car_->setDrivingModeFactory(driving_mode_factory_);
    has_driving_modes_ = true;
}
    
void MadCarBuilder::setSensor()
{
    if (has_proximity_sensor_) return;
    proximity_sensor_ = new Sensor(12);
    car_->setSensor(proximity_sensor_);
    has_proximity_sensor_ = true;
}
 
void MadCarBuilder::setState()
{
    if (has_state_) return;
    state_ = new State();
    car_->setState(state_);
    has_state_ = true;
}
    
void MadCarBuilder::setLogger()
{
    if (has_logger_) return;
    if (!(has_chassis_ || has_connector_)) return;
    logger_ = new Logger(controller_, 1000);
    car_->setLogger(logger_);
    has_logger_ = true;
}