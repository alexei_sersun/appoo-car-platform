#ifndef LOGGER_H_
#define LOGGER_H_

#include <stdint.h>
#include <AbstractLogger.h>
#include <Mediator.h>

class Logger : public AbstractLogger
{
private:
  Mediator* controller_;
  uint16_t timeout_;
  uint32_t last_log_time_;
  uint8_t* message_;

public:
  Logger(Mediator* controller, uint16_t timeout = 0);
  virtual ~Logger();
  void log();
  void timedLog();
};
#endif
