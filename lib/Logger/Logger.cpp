#include "Logger.h"
#include <Arduino.h>

Logger::Logger (Mediator* controller, uint16_t timeout)
{
  controller_ = controller;
  timeout_ = timeout;
  last_log_time_ = 0;
  message_ = (uint8_t*) malloc (9 * sizeof(uint8_t));
  message_[0] = 0xFA;
  message_[1] = 0xF1;
  message_[2] = 0x51;
  message_[3] = 0x02;
  message_[4] = 0x04;
  message_[5] = 0x04;
  message_[7] = 0x08;
}

Logger::~Logger ()
{
  free(message_);
}

void Logger::timedLog()
{
  if (last_log_time_ < millis() - timeout_ )
  {
    log();
    last_log_time_ = millis();
  }
}

void Logger::log()
{
  message_[6] = controller_->getSpeed();
  message_[8] = controller_->getSteering();
  controller_->writeToConnector(message_, 9);
}
