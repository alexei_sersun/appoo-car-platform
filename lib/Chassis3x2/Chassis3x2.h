#ifndef CHASSIS3x2_H_
#define CHASSIS3x2_H_

#include "Chassis.h"
#include "Wheel.h"
#include "DrivingMode.h"

class Chassis3x2 : public Chassis
{
private:
  DrivingMode* mode_;
  Wheel* left_wheel_;
  Wheel* right_wheel_;
  uint8_t speed_;
  uint8_t steering_direction_;

public:
  Chassis3x2 (uint8_t left_power_pin1, uint8_t left_power_pin2, uint8_t left_speed_pin,
      uint8_t right_power_pin1, uint8_t right_power_pin2, uint8_t right_speed_pin,
      DrivingMode* mode);
  virtual ~Chassis3x2 ();
  void stop();
  void setSpeed(uint8_t);
  void setDrivingMode(DrivingMode*);
  void steer(uint8_t);
  uint8_t getSpeed();
  uint8_t getSteering();
  void selfCheck();
};

#endif
