#include "Chassis3x2.h"

Chassis3x2::Chassis3x2 (uint8_t left_power_pin1, uint8_t left_power_pin2, uint8_t left_speed_pin,
      uint8_t right_power_pin1, uint8_t right_power_pin2, uint8_t right_speed_pin, DrivingMode* mode)
{
  left_wheel_ = new Wheel(left_power_pin1, left_power_pin2, left_speed_pin);
  right_wheel_ = new Wheel(right_power_pin1, right_power_pin2, right_speed_pin);
  mode_ = mode;
}

Chassis3x2::~Chassis3x2 ()
{
  delete left_wheel_;
  delete right_wheel_;
}

void Chassis3x2::stop()
{
  left_wheel_->stop();
  right_wheel_->stop();
}

void Chassis3x2::setSpeed(uint8_t speed)
{
  speed_ = speed;
  left_wheel_->setSpeed(speed_);
  right_wheel_->setSpeed(speed_);
}

void Chassis3x2::steer(uint8_t steering_coefficient)
{
  steering_direction_ = steering_coefficient;
  setSpeed(speed_);
}

void Chassis3x2::setDrivingMode(DrivingMode* mode)
{
  mode_ = mode;
}

uint8_t Chassis3x2::getSpeed()
{
  return speed_;
}

uint8_t Chassis3x2::getSteering()
{
  return steering_direction_;
}

void Chassis3x2::selfCheck()
{
  if (!mode_->canMoveForward())
  {
    stop();
  }
}
