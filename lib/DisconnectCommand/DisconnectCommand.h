#ifndef DISCONNECTCOMMAND_H_
#define DISCONNECTCOMMAND_H_

#include "Command.h"
#include "Mediator.h"

class DisconnectCommand : public Command
{
private:
  Mediator* controller_;

public:
  DisconnectCommand (Mediator* controller);
  virtual ~DisconnectCommand ();
  void execute();
};

#endif
