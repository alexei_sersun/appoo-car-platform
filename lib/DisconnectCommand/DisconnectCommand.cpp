#include "DisconnectCommand.h"
#include "DisconnectedCommandInterpreter.h"
#include <LogFactory.h>

DisconnectCommand::DisconnectCommand (Mediator* controller)
{
  controller_ = controller;
}

DisconnectCommand::~DisconnectCommand ()
{

}

void DisconnectCommand::execute()
{
  controller_->stopChassis();
  controller_->setState(0x0);
  controller_->registerCmdInterpreter(new DisconnectedCommandInterpreter(controller_));
}
