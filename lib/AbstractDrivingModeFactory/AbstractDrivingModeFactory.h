#ifndef ABSTRACTDRIVINGMODEFACTORY_H_
#define ABSTRACTDRIVINGMODEFACTORY_H_

#include <stdint.h>
#include "DrivingMode.h"

class AbstractDrivingModeFactory
{
public:
  AbstractDrivingModeFactory ();
  virtual ~AbstractDrivingModeFactory ();
  virtual DrivingMode* getInstance (uint8_t mode) = 0;
};

#endif
