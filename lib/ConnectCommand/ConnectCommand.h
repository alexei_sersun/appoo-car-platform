#ifndef CONNECTCOMMAND_H_
#define CONNECTCOMMAND_H_

#include "Command.h"
#include "Chassis.h"
#include "Connector.h"
#include "AbstractDrivingModeFactory.h"
#include "State.h"
#include "Mediator.h"

class ConnectCommand : public Command
{
private:
  Mediator* controller_;

public:
  ConnectCommand (Mediator*);
  virtual ~ConnectCommand ();
  void execute();
};

#endif
