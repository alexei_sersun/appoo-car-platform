#include "ConnectCommand.h"
#include "CommandInterpreter.h"

ConnectCommand::ConnectCommand (Mediator* controller)
{
  controller_ = controller;
}

ConnectCommand::~ConnectCommand ()
{

}

void ConnectCommand::execute()
{
  controller_->stopChassis();
  controller_->setDrivingMode(0x0);
  controller_->setState(State::CONNECTED);
  controller_->registerCmdInterpreter(new CommandInterpreter(controller_, 1));
}
