#ifndef AVOIDANCEMODE_H_
#define AVOIDANCEMODE_H_

#include "DrivingMode.h"
#include "Sensor.h"

class AvoidanceMode : public DrivingMode
{
private:
  Sensor* sensor_;

public:
  AvoidanceMode (Sensor*);
  virtual ~AvoidanceMode ();
  bool canMoveForward();
};

#endif
