#include "AvoidanceMode.h"

AvoidanceMode::AvoidanceMode (Sensor* sensor)
{
  sensor_ = sensor;
}

AvoidanceMode::~AvoidanceMode ()
{

}

bool AvoidanceMode::canMoveForward()
{
  return sensor_->readData() > 20;
}
