#ifndef MANUALMODE_H_
#define MANUALMODE_H_

#include "DrivingMode.h"

class ManualMode : public DrivingMode
{
public:
  ManualMode ();
  virtual ~ManualMode ();
  bool canMoveForward();
};

#endif
