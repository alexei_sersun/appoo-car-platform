#include <Arduino.h>
#include "CommandInterpreter.h"
#include "DrivingModeCommand.h"
#include "DummyCommand.h"
#include "SetSpeedCommand.h"
#include "SteerCommand.h"
#include "StopCommand.h"
#include "WrongCommand.h"
#include "ConnectCommand.h"
#include "DisconnectCommand.h"
#include "ExtendedLogDecorator.h"
#include "State.h"

Command* CommandInterpreter::parse(uint8_t* raw_data, uint8_t data_length)
{
  Serial.print("Received ");
  Serial.print(data_length);
  Serial.print(' ');
  for (uint8_t i = 0; i < data_length; i++) {
    Serial.print(raw_data[i], HEX);
  }
  Serial.println();

  if (!(4 < data_length && data_length < 7 && raw_data[0] == 0xFA &&
      raw_data[1] == 0xF1 && raw_data[2] == 0x51))
  {
    Serial.println("Wrong command");
    return log_factory_->decorate(new WrongCommand(controller_));
  }

  if (raw_data[4] == 0x0)
  {
    switch (raw_data[3]) {
      case 0x10: Serial.println("Stop");
        return log_factory_->decorate(new StopCommand(controller_));
      case 0x20: Serial.println("Disconnect");
        return log_factory_->decorate(new DisconnectCommand(controller_));
    }
  }
  else if (raw_data[4] == 0x1)
  {
    switch (raw_data[3]) {
      case 0x02: Serial.println("Set mode");
        return new DrivingModeCommand(controller_, raw_data[5]);
      case 0x04: Serial.println("Speed");
        return log_factory_->decorate(new SetSpeedCommand(controller_, raw_data[5]));
      case 0x08: Serial.println("Steer");
        return log_factory_->decorate(new SteerCommand(controller_, raw_data[5]));
    }
  }
  
  Serial.println("Wrong command");
  return log_factory_->decorate(new WrongCommand(controller_));
}

void CommandInterpreter::execute(Command* command)
{
  command->execute();
  delete command;
}

CommandInterpreter::CommandInterpreter (Mediator* controller, uint8_t log_level)
{
  controller_ = controller;
  log_factory_ = new LogFactory();
  log_factory_->setLogLevel(log_level);
}

CommandInterpreter::~CommandInterpreter ()
{
  delete log_factory_;
}

void CommandInterpreter::interpret(uint8_t* raw_data, uint8_t data_length)
{
  if (data_length == 0) return;
  Command* command = parse(raw_data, data_length);
  execute(command);
}
