#ifndef COMMANDINTERPRETER_H_
#define COMMANDINTERPRETER_H_

#include <stdint.h>
#include "Command.h"
#include "AbstractDrivingModeFactory.h"
#include <AbstractCmdInterpreter.h>
#include <Mediator.h>
#include <LogFactory.h>

class CommandInterpreter : public AbstractCmdInterpreter
{
private:
  LogFactory* log_factory_;
  Mediator* controller_;
  Command* parse(uint8_t* raw_data, uint8_t data_length);
  void execute(Command*);

public:
  
  CommandInterpreter(Mediator* mediator, uint8_t log_level = 0);
  virtual ~CommandInterpreter ();
  void interpret(uint8_t* raw_data, uint8_t data_length);
};

#endif
