#ifndef CHASSIS_H_
#define CHASSIS_H_

#include <stdint.h>
#include "DrivingMode.h"

class Chassis
{
public:
  Chassis ();
  virtual ~Chassis ();
  virtual void stop() = 0;
  virtual void setSpeed(uint8_t) = 0;
  virtual void setDrivingMode(DrivingMode*) = 0;
  virtual void steer(uint8_t) = 0;
  virtual uint8_t getSpeed() = 0;
  virtual uint8_t getSteering() = 0;
  virtual void selfCheck() = 0;
};

#endif
