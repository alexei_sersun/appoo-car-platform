#include "BTConnector.h"
#include <Arduino.h>

BTConnector::BTConnector (uint8_t rx_pin, uint8_t tx_pin)
{
  rx_pin_ = rx_pin;
  tx_pin_ = tx_pin;
  buffer_ = (uint8_t*) malloc(buffer_size_ * sizeof(uint8_t));
  pinMode(rx_pin_, INPUT);
  pinMode(tx_pin_, OUTPUT);
  bluetooth_ = new SoftwareSerial(rx_pin_, tx_pin_);
  bluetooth_->begin(9600);
}

BTConnector::~BTConnector ()
{
  delete bluetooth_;
}

int BTConnector::read(uint8_t** raw_data)
{
  delay(50);
  uint8_t buffer_ = 0;
  uint8_t index = 0;
  if (bluetooth_->available())
  {
    while (bluetooth_->available() > 0)
    {
      buffer_ = bluetooth_->read();
      *(*raw_data + index) = buffer_;
      ++index;
    }
  }
  return index;
}

int BTConnector::write(uint8_t* raw_data, uint8_t data_length)
{
  for (uint8_t i = 0; i < data_length; ++i)
  {
    bluetooth_->write(raw_data[i]);
  }
  return 0;
}
