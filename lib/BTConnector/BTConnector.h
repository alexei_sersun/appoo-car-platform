#ifndef BTCONNECTOR_H_
#define BTCONNECTOR_H_

#include <stdint.h>
#include <SoftwareSerial.h>
#include "Connector.h"

class BTConnector : public Connector
{
private:
  uint8_t rx_pin_;
  uint8_t tx_pin_;
  SoftwareSerial* bluetooth_;
  uint8_t* buffer_;
  const uint8_t buffer_size_ = 10;

public:
  BTConnector (uint8_t rx_pin, uint8_t tx_pin);
  virtual ~BTConnector ();
  int read(uint8_t** raw_data);
  int write(uint8_t* raw_data, uint8_t data_length);
};

#endif
