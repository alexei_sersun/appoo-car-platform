#ifndef CARBUILDER_H_
#define CARBUILDER_H_

#include "Car.h"

// Abstract class for car builders.
class CarBuilder
{
private:
    /* data */
public:
    CarBuilder(/* args */);
    virtual ~CarBuilder();
    virtual Car* getCar() = 0;
    virtual void setChassis() = 0;
    virtual void setController() = 0;
    virtual void setConnector() = 0;
    virtual void setCmdInterpreter() = 0;
    virtual void setDrivingModeFactory() = 0;
    virtual void setSensor() = 0;
    virtual void setState() = 0;
    virtual void setLogger() = 0;
};

#endif
