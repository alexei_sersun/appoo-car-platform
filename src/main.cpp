#include <Arduino.h>

#include <Car.h>
#include <MadCarBuilder.h>
#include <CarDirector.h>

CarBuilder* builder = new MadCarBuilder();
CarDirector* director = new CarDirector(builder);
Car* car;
void setup() {
  Serial.begin(9600);
  while (!Serial) {;}

  car = director->createCar();
}

void loop() {
  car->run();
}
