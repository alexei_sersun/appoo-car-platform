# Car Platform

Car Platform is a firmware to control an Arduino-powered device.

## Protocol

The following explains the protocol between Master device e.g. smartphone and Slave device like Arduino.

Master:
```
  <master-command> ::= <prefix><command><argument-flag>[<argument>]
  <prefix> ::= 0xFA 0xF1 0x51
  <command> ::= 0x1 | 0x2 | 0x4 | 0x8 | 0x16
  <argument-flag> ::= 0x0 | 0x1
  <argument> ::= <8-bit value>
```

Here, the `command` meaning is the following:
  - `0x1` - Connect
  - `0x2` - Set mode
  - `0x4` - Set speed
  - `0x8` - Set steering direction
  - `0x16` - Stop
  - `0x32` - Disconnect

The `argument-flag` is required to be specified when the command transmits an argument.

Slave:
```
  <slave-response> ::= <prefix><response-type><response-length><response-body>
  <prefix> ::= 0xFA 0xF1 0x51
  <response-type> ::= 0x1 | 0x2 | 0x4
  <response-length> ::= <8-bit value>
  <response-body> ::= <bit-array>
```

Here, the `response-type` means:
  - `0x1` - OK
  - `0x2` - Log info
  - `0x4` - Error

### Connection
Master initializes the connection.

Master command:
```
  0xFA 0xF1 0x51 0x1 0x0
```

Slave response, if the connection is done well:
```
  0xFA 0xF1 0x51 0x1 0x0
```

### Disconnection
Master initializes the disconnection.

Master command:
```
  0xFA 0xF1 0x51 0x32 0x0
```

Slave response, if the connection is done well:
```
  0xFA 0xF1 0x51 0x1 0x0
```

### Steering & moving

The car is moved using 2 commands: Set steering direction (`0x8`) and Set speed (`0x4`).

#### Set speed

Master command:
```
  0xFA 0xF1 0x51 0x4 0x1 0x__
```

Here, the values of `0x__` vary from `0x0` (0, maximal speed backward) to `0xFF` (255, maximal speed forward).

Slave response:
```
  0xFA 0xF1 0x51 0x1 0x0
```

#### Set steering direction

Master command:
```
  0xFA 0xF1 0x51 0x8 0x1 0x__
```

Here, the values of `0x__` vary from `0x0` (0, turn leftmost) to `0xFF` (255, turn rightmost).

Slave response:
```
  0xFA 0xF1 0x51 0x1 0x0
```

### Driving modes

The car has 2 modes of driving: manual and avoidance mode.

In Manual mode (`0xA0`), the car platform executes every command solely as the user wishes it to move, no matter whether there are obstacles in front of it or not.

In Avoidance mode (`0xA1`), the car platform has to use a sensor to detect obstacles in front of it.

By default, the car platform is in Manual mode.

Master command to set a given mode:
```
  0xFA 0xF1 0x51 0x2 0x1 0x__
```

Here, the values of `0x__` are `0xA0` for Manual mode and `0xA1` for Avoidance mode.

Slave response:
```
  0xFA 0xF1 0x51 0x1 0x0
```
